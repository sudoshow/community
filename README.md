# How can the community contribute to the Sudo Show?

This project is for submitting show ideas using Gitlab Issues.  If you have episode ideas please create an issue in this project! [Create Issue](https://gitlab.com/sudoshow/community/-/issues)

Rules:
- 1 - Nothing NSFW
- 2 - Keep the language professional
- 3 - Try to keep the topic to "open souce in business"
    - If you submit a topic that doesn't fit it could still get used either by Sudo Show or other podcasts in DLN
    - Sudo Show may be doing topics out side of this in the near future but it will be limited
